# AuthorizationService

Сервис аутентификации и авторизации.

## Настройка окружения
Для корректного запуска сервиса на компьютере, на котором планируется запуск, необходимо развернуть компоненты:
1. PostgreSQL 11
2. RabbitMQ

Для простой и быстрой настройки окружения используется docker-compose. Для того, чтобы развернуть окружение, необходимое для работы сервиса нужно:
1. Открыть папку .\Docker\ в powershell.
2. Выполнить команду 
```
docker-compose up -d --build
``` 
Docker-compose поднимет два контейнера: RabbitMQ, PostgresSql доступных по адресам
- RabbitMQ - http://127.0.0.1:15672 (login: guest, password: guest)
- PostgreSQL - 127.0.0.1 порт 5432 (login: postgres, password: Password10)
## Построение сервиса
Для построения сервиса необходимо запустить скрип построения командой

```
./build.ps1
```
## Развертывание базы данных
Для поставки изменений базы данных рекомендуется использовать Entity Framework Mirgrations. 

Для настройки использования миграций необходимо предварительно установить на сервер/разработческий компьютер EF Core .NET CLI tools так как описано в [инструкуции](https://docs.microsoft.com/ru-ru/ef/core/miscellaneous/cli/dotnet).

Для того, чтобы развернуть базу данных необходимо в консоли (powershell/cmd) выполнить команду:
```
dotnet ef database --startup-project "Sources\AuthorizationService.Instance\AuthorizationService.Instance.csproj" --project "Sources\AuthorizationService.Data\AuthorizationService.Data.csproj" update
```
## Запуск сервиса

В процессе построения в папке ```./artifacts/packages``` будет создан NuGet пакет с исполняемыми файлами сервиса.

Для запуска сервиса необходимо извлечь содержимое NuGet пакета и выполнить команду

```
dotnet lib/netcoreapp2.2/AuthorizationService.Instance.dll
```

или запустить исполняемые файлы, созданные в процессе посторения командой

```
dotnet sources/AuthorizationService.Instance/release/netcoreapp2.2/AuthorizationService.Instance.dll
```

После успешного запуска сервиса должнен быть доступен следующий метод: 

- ```GET localhost:5000/health``` 

В случае успешного запуска сервиса будет получен ответ ```200 - OK```. 

## Описание веб методов

Описание веб методов сервиса доступны по [ссылке](http://localhost:5000/swagger/index.html) при условии, что сервис запущен. В случае, если сервис развернут не на локальной машине, то необходимо заменить адрес сервиса и номер порта на соответствующие. 
