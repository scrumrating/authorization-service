﻿using AuthorizationService.Consumers.Authorizations;
using AuthorizationService.Core.Dtos;
using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Services;
using FakeItEasy;
using MassTransit;
using Microsoft.Extensions.Logging;
using NUnit.Framework;

namespace AuthorizationService.Consumers.Tests
{
    internal class AuthorizationConsumerTests
    {
        [Test]
        [Description("Авторизация завершается с ошибкой, если пользователь не зарегистрирован.")]
        public void CanRespondWithNotFound()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };

            IUserService userService = A.Fake<IUserService>();
            ITokenService tokenService = A.Fake<ITokenService>();
            ILogger<AuthorizationConsumer> logger = A.Fake<ILogger<AuthorizationConsumer>>();
            ConsumeContext<AuthorizationCommand> context = A.Fake<ConsumeContext<AuthorizationCommand>>();
            AuthorizationCommand command = new AuthorizationCommand { User = userDto };

            A.CallTo(() => context.Message).Returns(command);
            A.CallTo(() => userService.GetAsync(A<string>._, A<string>._)).Returns<User>(null);

            AuthorizationConsumer sut = new AuthorizationConsumer(userService, tokenService, logger);
            sut.Consume(context).GetAwaiter().GetResult();

            A.CallTo(() => context.RespondAsync(A<AuthorizationResponse>
                .That.Matches(r => r.Result == "not-found"))).MustHaveHappenedOnceExactly();
        }

        [Test]
        [Description("Если пользователь зарегистрирован, то авторизация проходит успешно и возвращается существующий валидный токен.")]
        public void CanRespondWithSeccessAndCurrentValidToken()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            User user = new User(userDto.Login, userDto.Password);
            string token = "oldToken";

            IUserService userService = A.Fake<IUserService>();
            ITokenService tokenService = A.Fake<ITokenService>();
            ILogger<AuthorizationConsumer> logger = A.Fake<ILogger<AuthorizationConsumer>>();
            ConsumeContext<AuthorizationCommand> context = A.Fake<ConsumeContext<AuthorizationCommand>>();
            AuthorizationCommand command = new AuthorizationCommand { User = userDto };

            A.CallTo(() => context.Message).Returns(command);
            A.CallTo(() => userService.GetAsync(A<string>._, A<string>._)).Returns(user);
            A.CallTo(() => tokenService.GetAsync(A<User>._)).Returns(token);

            AuthorizationConsumer sut = new AuthorizationConsumer(userService, tokenService, logger);
            sut.Consume(context).GetAwaiter().GetResult();

            A.CallTo(() => context.RespondAsync(A<AuthorizationResponse>
                .That.Matches(r => r.Token.Token == token && r.Result == "success"))).MustHaveHappenedOnceExactly();
        }

        [Test]
        [Description("Если пользователь зарегистрирован, то авторизация проходит успешно и возвращается новый валидный токен.")]
        public void CanRespondWithSeccessAndNewValidToken()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            User user = new User(userDto.Login, userDto.Password);
            string token = "newToken";

            IUserService userService = A.Fake<IUserService>();
            ITokenService tokenService = A.Fake<ITokenService>();
            ILogger<AuthorizationConsumer> logger = A.Fake<ILogger<AuthorizationConsumer>>();
            ConsumeContext<AuthorizationCommand> context = A.Fake<ConsumeContext<AuthorizationCommand>>();
            AuthorizationCommand command = new AuthorizationCommand { User = userDto };

            A.CallTo(() => context.Message).Returns(command);
            A.CallTo(() => userService.GetAsync(A<string>._, A<string>._)).Returns(user);
            A.CallTo(() => tokenService.GetAsync(A<User>._)).Returns<string>(null);
            A.CallTo(() => tokenService.Generate(A<User>._)).Returns(token);

            AuthorizationConsumer sut = new AuthorizationConsumer(userService, tokenService, logger);
            sut.Consume(context).GetAwaiter().GetResult();

            A.CallTo(() => context.RespondAsync(A<AuthorizationResponse>
                .That.Matches(r => r.Token.Token == token && r.Result == "success"))).MustHaveHappenedOnceExactly();
        }
    }
}