﻿using AuthorizationService.Consumers.Authentications;
using AuthorizationService.Core.Dtos;
using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Services;
using FakeItEasy;
using MassTransit;
using Microsoft.Extensions.Logging;
using NUnit.Framework;

namespace AuthorizationService.Consumers.Tests
{
    internal class AuthenticationConsumerTests
    {
        [Test]
        [Description("Аутентификация проходит успешно, если пользователь еще не зарегистрирован.")]
        public void CanRespondWithSuccess()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            string token = "testToken";
            IUserService userService = A.Fake<IUserService>();
            ITokenService tokenService = A.Fake<ITokenService>();
            ILogger<AuthenticationConsumer> logger = A.Fake<ILogger<AuthenticationConsumer>>();
            ConsumeContext<AuthenticationCommand> context = A.Fake<ConsumeContext<AuthenticationCommand>>();
            AuthenticationCommand command = new AuthenticationCommand { User = userDto };

            A.CallTo(() => context.Message).Returns(command);
            A.CallTo(() => userService.GetAsync(A<string>._)).Returns<User>(null);
            A.CallTo(() => tokenService.Generate(A<User>._)).Returns(token);

            AuthenticationConsumer sut = new AuthenticationConsumer(userService, tokenService, logger);
            sut.Consume(context).GetAwaiter().GetResult();

            A.CallTo(() => context.RespondAsync(A<AuthenticationResponse>
                .That.Matches(r => r.Token.Token == token && r.Result == "success"))).MustHaveHappenedOnceExactly();
        }

        [Test]
        [Description("Аутентификация завершается с ошибкой, если пользователь уже зарегистрирован.")]
        public void CanRespondWithConflict()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            User user = new User(userDto.Login, userDto.Password);
            IUserService userService = A.Fake<IUserService>();
            ITokenService tokenService = A.Fake<ITokenService>();
            ILogger<AuthenticationConsumer> logger = A.Fake<ILogger<AuthenticationConsumer>>();
            ConsumeContext<AuthenticationCommand> context = A.Fake<ConsumeContext<AuthenticationCommand>>();
            AuthenticationCommand command = new AuthenticationCommand { User = userDto };

            A.CallTo(() => context.Message).Returns(command);
            A.CallTo(() => userService.GetAsync(A<string>._)).Returns(user);

            AuthenticationConsumer sut = new AuthenticationConsumer(userService, tokenService, logger);
            sut.Consume(context).GetAwaiter().GetResult();

            A.CallTo(() => context.RespondAsync(A<AuthenticationResponse>
                .That.Matches(r => r.Result == "conflict"))).MustHaveHappenedOnceExactly();
        }
    }
}