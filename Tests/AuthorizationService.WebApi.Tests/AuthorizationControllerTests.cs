﻿using AuthorizationService.Consumers.Authorizations;
using AuthorizationService.Core.Dtos;
using AuthorizationService.WebApi.Controllers;
using FakeItEasy;
using FluentAssertions;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Net;
using System.Threading;

namespace AuthorizationService.WebApi.Tests
{
    internal class AuthorizationControllerTests
    {
        [Test]
        [Description("Запрос авторизации должен завершаться успешно (код ответа 200, вернулся токен).")]
        public void CanAuthorizeWithHttpStatusCode200()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            TokenDto tokenDto = new TokenDto("testToken");
            IRequestClient<AuthorizationCommand> client = A.Fake<IRequestClient<AuthorizationCommand>>();
            Response<AuthorizationResponse> response = A.Fake<Response<AuthorizationResponse>>();

            A.CallTo(() => response.Message).Returns(new AuthorizationResponse { Token = tokenDto, Result = "success" });
            A.CallTo(() => client.GetResponse<AuthorizationResponse>(
                    A<AuthorizationCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Returns(response);

            AuthorizationController sut = new AuthorizationController(client);
            var result = sut.Put(userDto).GetAwaiter().GetResult().Result as OkObjectResult;

            TokenDto token = result.Value as TokenDto;
            int? statusCode = result.StatusCode;

            statusCode.Should().NotBeNull();
            statusCode.Should().Be((int)HttpStatusCode.OK);
            token.Should().NotBeNull();
            token.Token.Should().Be(tokenDto.Token);
        }

        [Test]
        [Description("Запрос авторизации должен завершаться cо ошибкой (код ответа 502 BadGateway).")]
        public void CanAuthorizeWithHttpStatusCode502()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            IRequestClient<AuthorizationCommand> client = A.Fake<IRequestClient<AuthorizationCommand>>();
            Response<AuthorizationResponse> response = A.Fake<Response<AuthorizationResponse>>();

            A.CallTo(() => response.Message).Returns(new AuthorizationResponse());
            A.CallTo(() => client.GetResponse<AuthorizationResponse>(
                    A<AuthorizationCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Returns(response);

            AuthorizationController sut = new AuthorizationController(client);
            var result = sut.Put(userDto).GetAwaiter().GetResult().Result as StatusCodeResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadGateway);
        }

        [Test]
        [Description("Запрос авторизации должен завершаться cо ошибкой (код ответа 504 GatewayTimeout).")]
        public void CanAuthorizeWithHttpStatusCode504()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            IRequestClient<AuthorizationCommand> client = A.Fake<IRequestClient<AuthorizationCommand>>();

            A.CallTo(() => client.GetResponse<AuthorizationResponse>(
                    A<AuthorizationCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Throws(new RequestTimeoutException());

            AuthorizationController sut = new AuthorizationController(client);
            var result = sut.Put(userDto).GetAwaiter().GetResult().Result as StatusCodeResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.GatewayTimeout);
        }

        [Test]
        [Description("Запрос авторизации должен завершаться cо ошибкой (код ответа 502 GatewayTimeout), если при обработке запроса произошла непредвиденная ошибка.")]
        public void CanAuthorizeWithHttpStatusCode502WhenInternalErrorOccured()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            IRequestClient<AuthorizationCommand> client = A.Fake<IRequestClient<AuthorizationCommand>>();

            A.CallTo(() => client.GetResponse<AuthorizationResponse>(
                    A<AuthorizationCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Throws(new Exception());

            AuthorizationController sut = new AuthorizationController(client);
            var result = sut.Put(userDto).GetAwaiter().GetResult().Result as StatusCodeResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadGateway);
        }
    }
}