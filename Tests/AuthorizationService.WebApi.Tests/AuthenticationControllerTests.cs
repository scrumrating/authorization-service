﻿using AuthorizationService.Consumers.Authentications;
using AuthorizationService.Core.Dtos;
using AuthorizationService.WebApi.Controllers;
using FakeItEasy;
using FluentAssertions;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Net;
using System.Threading;

namespace AuthorizationService.WebApi.Tests
{
    internal class AuthenticationControllerTests
    {
        [Test]
        [Description("Запрос аутентификации должен завершаться успешно (код ответа 200, вернулся токен).")]
        public void CanAuthenticateWithHttpStatusCode200()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            TokenDto tokenDto = new TokenDto("testToken");
            IRequestClient<AuthenticationCommand> client = A.Fake<IRequestClient<AuthenticationCommand>>();
            Response<AuthenticationResponse> response = A.Fake<Response<AuthenticationResponse>>();

            A.CallTo(() => response.Message).Returns(new AuthenticationResponse { Token = tokenDto, Result = "success" });
            A.CallTo(() => client.GetResponse<AuthenticationResponse>(
                    A<AuthenticationCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Returns(response);

            AuthenticationController sut = new AuthenticationController(client);
            var result = sut.Post(userDto).GetAwaiter().GetResult().Result as OkObjectResult;

            TokenDto token = result.Value as TokenDto;
            int? statusCode = result.StatusCode;

            statusCode.Should().NotBeNull();
            statusCode.Should().Be((int)HttpStatusCode.OK);
            token.Should().NotBeNull();
            token.Token.Should().Be(tokenDto.Token);
        }

        [Test]
        [Description("Запрос аутентификации должен завершаться cо ошибкой (код ответа 409 Conflict), если аутентифицируемый пользователь уже зарегистрирован.")]
        public void CanAuthenticateWithHttpStatusCode409()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            IRequestClient<AuthenticationCommand> client = A.Fake<IRequestClient<AuthenticationCommand>>();
            Response<AuthenticationResponse> response = A.Fake<Response<AuthenticationResponse>>();

            A.CallTo(() => response.Message).Returns(new AuthenticationResponse { Result = "conflict" });
            A.CallTo(() => client.GetResponse<AuthenticationResponse>(
                    A<AuthenticationCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Returns(response);

            AuthenticationController sut = new AuthenticationController(client);
            var result = sut.Post(userDto).GetAwaiter().GetResult().Result as StatusCodeResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.Conflict);
        }

        [Test]
        [Description("Запрос аутентификации должен завершаться cо ошибкой (код ответа 502 BadGateway).")]
        public void CanAuthenticateWithHttpStatusCode502()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            IRequestClient<AuthenticationCommand> client = A.Fake<IRequestClient<AuthenticationCommand>>();
            Response<AuthenticationResponse> response = A.Fake<Response<AuthenticationResponse>>();

            A.CallTo(() => response.Message).Returns(new AuthenticationResponse());
            A.CallTo(() => client.GetResponse<AuthenticationResponse>(
                    A<AuthenticationCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Returns(response);

            AuthenticationController sut = new AuthenticationController(client);
            var result = sut.Post(userDto).GetAwaiter().GetResult().Result as StatusCodeResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadGateway);
        }

        [Test]
        [Description("Запрос аутентификации должен завершаться cо ошибкой (код ответа 504 GatewayTimeout).")]
        public void CanAuthenticateWithHttpStatusCode504()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            IRequestClient<AuthenticationCommand> client = A.Fake<IRequestClient<AuthenticationCommand>>();

            A.CallTo(() => client.GetResponse<AuthenticationResponse>(
                    A<AuthenticationCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Throws(new RequestTimeoutException());

            AuthenticationController sut = new AuthenticationController(client);
            var result = sut.Post(userDto).GetAwaiter().GetResult().Result as StatusCodeResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.GatewayTimeout);
        }

        [Test]
        [Description("Запрос аутентификации должен завершаться cо ошибкой (код ответа 502 GatewayTimeout), если при обработке запроса произошла непредвиденная ошибка.")]
        public void CanAuthenticateWithHttpStatusCode502WhenInternalErrorOccured()
        {
            UserDto userDto = new UserDto { Login = "JhonDoe", Password = "guest" };
            IRequestClient<AuthenticationCommand> client = A.Fake<IRequestClient<AuthenticationCommand>>();

            A.CallTo(() => client.GetResponse<AuthenticationResponse>(
                    A<AuthenticationCommand>._,
                    A<CancellationToken>._,
                    A<RequestTimeout>._))
                .Throws(new Exception());

            AuthenticationController sut = new AuthenticationController(client);
            var result = sut.Post(userDto).GetAwaiter().GetResult().Result as StatusCodeResult;

            result.Should().NotBeNull();
            result.StatusCode.Should().Be((int)HttpStatusCode.BadGateway);
        }
    }
}