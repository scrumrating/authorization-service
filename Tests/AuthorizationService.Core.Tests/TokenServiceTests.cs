﻿using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Hashers;
using AuthorizationService.Core.Repositories;
using AuthorizationService.Core.Services;
using AuthorizationService.Core.Tokens;
using FakeItEasy;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;

namespace AuthorizationService.Core.Tests
{
    internal class TokenServiceTests
    {
        [Test]
        [Description("Получение токена пользователя проходит успешно.")]
        public void CanGetToken()
        {
            IRepository<Token, int> repository = A.Fake<IRepository<Token, int>>();
            IEncodingKey encodingKey = A.Fake<IEncodingKey>();
            ILogger<TokenService> logger = A.Fake<ILogger<TokenService>>();
            User user = new User("JhonDoe", "guest") { Id = 1 };
            Token token = new Token { Value = "testToken" };
            int tokenLifetime = 5;

            A.CallTo(() => repository.FindAsync(user.Id)).Returns(token);

            TokenService sut = new TokenService(repository, encodingKey, logger, tokenLifetime);
            string result = sut.GetAsync(user).GetAwaiter().GetResult();

            result.Should().NotBeNull();
            result.Should().Be(token.Value);
        }
    }
}