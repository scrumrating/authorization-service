﻿using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Hashers;
using AuthorizationService.Core.Repositories;
using AuthorizationService.Core.Services;
using FakeItEasy;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NUnit.Framework;

namespace AuthorizationService.Core.Tests
{
    internal class UserServiceTests
    {
        [Test]
        [Description("Получение данных пользователя по логину проходит успешно.")]
        public void CanGetUserByLogin()
        {
            IRepository<User, string> repository = A.Fake<IRepository<User, string>>();
            IHasher hasher = A.Fake<IHasher>();
            ILogger<UserService> logger = A.Fake<ILogger<UserService>>();
            User user = new User("JhonDoe", "guest");

            A.CallTo(() => repository.FindAsync(user.Login)).Returns(user);

            UserService sut = new UserService(repository, hasher, logger);
            var result = sut.GetAsync(user.Login).GetAwaiter().GetResult();

            result.Should().NotBeNull();
            result.Should().Be(user);
        }

        [Test]
        [Description("Получение данных зарегистрированного пользователя по логину и паролю проходит успешно.")]
        public void CanGetUserByValidLoginAndPassword()
        {
            IRepository<User, string> repository = A.Fake<IRepository<User, string>>();
            IHasher hasher = A.Fake<IHasher>();
            ILogger<UserService> logger = A.Fake<ILogger<UserService>>();
            User user = new User("JhonDoe", "guest");

            A.CallTo(() => repository.FindAsync(user.Login)).Returns(user);
            A.CallTo(() => hasher.GetHash(user.Password)).Returns(user.Password);

            UserService sut = new UserService(repository, hasher, logger);
            var result = sut.GetAsync(user.Login, user.Password).GetAwaiter().GetResult();

            result.Should().NotBeNull();
            result.Should().Be(user);
        }

        [Test]
        [Description("Получение данных не зарегистрированного пользователя по логину и паролю не проходит.")]
        public void CanNotGetUserByNotValidLoginOrPassword()
        {
            IRepository<User, string> repository = A.Fake<IRepository<User, string>>();
            IHasher hasher = A.Fake<IHasher>();
            ILogger<UserService> logger = A.Fake<ILogger<UserService>>();
            User user = new User("JhonDoe", "guest");
            string passwordHash = "testHash";

            A.CallTo(() => repository.FindAsync(user.Login)).Returns(user);
            A.CallTo(() => hasher.GetHash(user.Password)).Returns(passwordHash);

            UserService sut = new UserService(repository, hasher, logger);
            var result = sut.GetAsync(user.Login, user.Password).GetAwaiter().GetResult();

            result.Should().BeNull();
        }

        [Test]
        [Description("Сохранение данных пользователя проходит успешно.")]
        public void CanSaveUser()
        {
            IRepository<User, string> repository = A.Fake<IRepository<User, string>>();
            IHasher hasher = A.Fake<IHasher>();
            ILogger<UserService> logger = A.Fake<ILogger<UserService>>();
            User user = new User("JhonDoe", "guest");

            A.CallTo(() => repository.AddAsync(user)).Returns(1);

            UserService sut = new UserService(repository, hasher, logger);
            int result = sut.SaveAsync(user).GetAwaiter().GetResult();

            result.Should().Be(1);
        }
    }
}