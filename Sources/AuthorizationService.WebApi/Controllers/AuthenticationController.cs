﻿using AuthorizationService.Consumers.Authentications;
using AuthorizationService.Core.Dtos;
using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AuthorizationService.WebApi.Controllers
{
    /// <summary>
    /// Контроллер аутентификации.
    /// </summary>
    [Route("api/authentication")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IRequestClient<AuthenticationCommand> client;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AuthenticationController"/>.
        /// </summary>
        /// <param name="authenticationClient">Клиент для обработки команд аутентификации.</param>
        public AuthenticationController(IRequestClient<AuthenticationCommand> authenticationClient)
        {
            this.client = authenticationClient;
        }

        /// <summary>
        /// Метод для получения доступных методов.
        /// </summary>
        /// <returns>Список доступных методов.</returns>
        [HttpOptions]
        public ActionResult Options()
        {
            this.Response.Headers.Add("Allow", "POST, OPTIONS");
            return this.Ok();
        }

        /// <summary>
        /// Зарегистрировать пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns>Токен.</returns>
        /// <response code="200">Аутентификация пройдена.</response>
        /// <response code="409">Пользователь с таким логином уже существует.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.Conflict)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<ActionResult<TokenDto>> Post([FromBody] UserDto user)
        {
            try
            {
                Response<AuthenticationResponse> response = await this.client.GetResponse<AuthenticationResponse>(new AuthenticationCommand { User = user });

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Token);
                    case "conflict":
                        return new StatusCodeResult((int)HttpStatusCode.Conflict);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
    }
}