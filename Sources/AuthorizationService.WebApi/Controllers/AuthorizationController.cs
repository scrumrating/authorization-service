﻿using AuthorizationService.Consumers.Authorizations;
using AuthorizationService.Core.Dtos;
using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AuthorizationService.WebApi.Controllers
{
    /// <summary>
    /// Контроллер авторизации.
    /// </summary>
    [Route("api/authorization")]
    public class AuthorizationController : ControllerBase
    {
        private readonly IRequestClient<AuthorizationCommand> client;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AuthorizationController"/>.
        /// </summary>
        /// <param name="authorizationClient">Клиент для обработки команд авторизации.</param>
        public AuthorizationController(IRequestClient<AuthorizationCommand> authorizationClient)
        {
            this.client = authorizationClient;
        }

        /// <summary>
        /// Метод для получения доступных методов.
        /// </summary>
        /// <returns>Список доступных методов.</returns>
        [HttpOptions]
        public ActionResult Options()
        {
            this.Response.Headers.Add("Allow", "PUT, OPTIONS");
            return this.Ok();
        }

        /// <summary>
        /// Авторизовать пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns>Токен.</returns>
        /// <response code="200">Пользователь авторизован.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [AllowAnonymous]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<ActionResult<TokenDto>> Put([FromBody] UserDto user)
        {
            try
            {
                Response<AuthorizationResponse> response = await this.client.GetResponse<AuthorizationResponse>(new AuthorizationCommand { User = user });

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Token);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
    }
}