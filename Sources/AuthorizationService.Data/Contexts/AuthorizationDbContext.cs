﻿using AuthorizationService.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationService.Data.Contexts
{
    /// <summary>
    /// Конткст доступа к данным сервиса.
    /// </summary>
    public class AuthorizationDbContext : DbContext
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AuthorizationDbContext"/>.
        /// </summary>
        public AuthorizationDbContext(DbContextOptions<AuthorizationDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Получает или задает набор данных пользователей.
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Получает или задает набор токены.
        /// </summary>
        public DbSet<Token> Tokens { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var userEntity = modelBuilder.Entity<User>();

            userEntity.ToTable("User").HasKey(k => k.Id);

            userEntity.Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            userEntity.Property(p => p.Login).IsRequired();
            userEntity.Property(p => p.Password).IsRequired();
            userEntity.Property(p => p.CreatedOn).ValueGeneratedOnAdd();

            var tokenEntity = modelBuilder.Entity<Token>();

            tokenEntity.ToTable("Token").HasKey(k => k.Id);

            tokenEntity.Property(p => p.Id).ValueGeneratedOnAdd();
            tokenEntity.Property(p => p.Value).IsRequired();
            tokenEntity.Property(p => p.ValidTo).IsRequired();
        }
    }
}