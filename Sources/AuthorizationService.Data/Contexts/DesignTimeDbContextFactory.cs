﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace AuthorizationService.Data.Contexts
{
    /// <summary>
    /// Фабрика для генераци контекста, необходимого для создания миграций.
    /// </summary>
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AuthorizationDbContext>
    {
        /// <inheritdoc/>
        public AuthorizationDbContext CreateDbContext(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("appsettings.production.json", true, true)
                .Build();

            var builder = new DbContextOptionsBuilder<AuthorizationDbContext>();
            builder.UseNpgsql(configuration.GetConnectionString("AuthorizationService"));
            return new AuthorizationDbContext(builder.Options);
        }
    }
}