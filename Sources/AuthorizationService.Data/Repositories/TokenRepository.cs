﻿using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Repositories;
using AuthorizationService.Data.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationService.Data.Repositories
{
    /// <summary>
    /// Репозиторий для взаимодействия с данными токенов.
    /// </summary>
    public class TokenRepository : IRepository<Token, int>
    {
        private readonly AuthorizationDbContext context;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TokenRepository"/>.
        /// </summary>
        /// <param name="context">Контекст доступа к данным сервиса.</param>
        public TokenRepository(AuthorizationDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Сохранить токен в базе данных.
        /// </summary>
        /// <param name="token">Токен <see cref="Token"/>.</param>
        /// <returns>Идентификатор токена.</returns>
        public int Add(Token token)
        {
            var entityEntry = this.context.Tokens.Add(token);
            this.context.SaveChanges();

            return entityEntry.Entity.Id;
        }

        /// <summary>
        /// Сохранить токен в базе данных.
        /// </summary>
        /// <param name="token">Токен <see cref="Token"/>.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task<int> AddAsync(Token token)
        {
            var entityEntry = this.context.Tokens.Add(token);
            await this.context.SaveChangesAsync();

            return entityEntry.Entity.Id;
        }

        /// <summary>
        /// Найти валидный токен для пользователя.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя, для которого необходимо найти валидный токен.</param>
        /// <returns>Токен <see cref="Token"/>.</returns>
        public Token Find(int userId)
        {
            return this.context.Tokens.LastOrDefault(t => t.UserId == userId && t.ValidTo > DateTime.Now);
        }

        /// <summary>
        /// Найти валидный токен для пользователя.
        /// </summary>
        /// <param name="userId">Идентификатор пользователя, для которого необходимо найти валидный токен.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task<Token> FindAsync(int userId)
        {
            return await Task.Run(() => this.context.Tokens.LastOrDefault(t => t.UserId == userId && t.ValidTo > DateTime.Now));
        }
    }
}