﻿using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Repositories;
using AuthorizationService.Data.Contexts;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationService.Data.Repositories
{
    /// <summary>
    /// Репозиторий для взаимодействия с данными пользователей.
    /// </summary>
    public class UsersRepository : IRepository<User, string>
    {
        private readonly AuthorizationDbContext context;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="UsersRepository"/>.
        /// </summary>
        /// <param name="context">Контекст доступа к данным сервиса.</param>
        public UsersRepository(AuthorizationDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Добавить одного пользователя в базу данных.
        /// </summary>
        /// <param name="user">Контакт.</param>
        /// <returns>Идентификатор пользователя.</returns>
        public int Add(User user)
        {
            var entityEntry = this.context.Users.Add(user);
            this.context.SaveChanges();

            return entityEntry.Entity.Id;
        }

        /// <summary>
        /// Добавить одного пользователя в базу данных.
        /// </summary>
        /// <param name="user">Контакт.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task<int> AddAsync(User user)
        {
            var entityEntry = this.context.Users.Add(user);
            await this.context.SaveChangesAsync();

            return entityEntry.Entity.Id;
        }

        /// <summary>
        /// Найти пользователя по его логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <returns>Пользователь.</returns>
        public User Find(string login)
        {
            return this.context.Users.FirstOrDefault(u => u.Login.Equals(login));
        }

        /// <summary>
        /// Найти пользователя по его логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <returns>Пользователь.</returns>
        public async Task<User> FindAsync(string login)
        {
            return await Task.Run(() => this.context.Users.FirstOrDefault(u => u.Login.Equals(login)));
        }
    }
}