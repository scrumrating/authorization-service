﻿using AuthorizationService.Core.Dtos;
using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Services;
using MassTransit;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AuthorizationService.Consumers.Authorizations
{
    /// <summary>
    /// Обработчик сообщения авторизации пользователя.
    /// </summary>
    public class AuthorizationConsumer : IConsumer<AuthorizationCommand>
    {
        private readonly IUserService userService;
        private readonly ITokenService tokenService;
        private readonly ILogger<AuthorizationConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AuthorizationConsumer"/>.
        /// </summary>
        /// <param name="userService">Сервис для работы с данными пользователей.</param>
        /// <param name="tokenService">Сервис для работы с данными токенов.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public AuthorizationConsumer(IUserService userService, ITokenService tokenService, ILogger<AuthorizationConsumer> logger)
        {
            this.userService = userService;
            this.tokenService = tokenService;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<AuthorizationCommand> context)
        {
            UserDto userFromRequest = context.Message.User;
            this.logger.LogInformation($"Выполняется обработка сообщения авторизации пользователя с логином '{userFromRequest.Login}'.");

            // Проверяем зарегистрирован ли пользователь с таким логином и паролем.
            User existingUser = await this.userService.GetAsync(userFromRequest.Login, userFromRequest.Password);

            if (existingUser == null)
            {
                await context.RespondAsync(new AuthorizationResponse { Result = "not-found" });
            }
            else
            {
                // Получаем для пользователя валидный токен.
                string tokenValue = await this.tokenService.GetAsync(existingUser);

                // Если валидный токен истек, то генерируем новый.
                if (tokenValue == null)
                {
                    tokenValue = this.tokenService.Generate(existingUser);
                }

                await context.RespondAsync(new AuthorizationResponse { Token = new TokenDto(tokenValue), Result = "success" });
            }
        }
    }
}