﻿using AuthorizationService.Core.Dtos;

namespace AuthorizationService.Consumers.Authorizations
{
    /// <summary>
    /// Команда авторизации пользователя.
    /// </summary>
    public class AuthorizationCommand
    {
        /// <summary>
        /// Получает или задает пользователя.
        /// </summary>
        public UserDto User { get; set; }
    }
}