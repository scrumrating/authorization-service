﻿using AuthorizationService.Core.Dtos;

namespace AuthorizationService.Consumers.Authorizations
{
    /// <summary>
    /// Ответ на команду авторизации пользователя.
    /// </summary>
    public class AuthorizationResponse
    {
        /// <summary>
        /// Получает или задает токен.
        /// </summary>
        public TokenDto Token { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}