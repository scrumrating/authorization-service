﻿using AuthorizationService.Core.Dtos;
using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Services;
using MassTransit;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AuthorizationService.Consumers.Authentications
{
    /// <summary>
    /// Обработчик сообщения аутентификации пользователя.
    /// </summary>
    public class AuthenticationConsumer : IConsumer<AuthenticationCommand>
    {
        private readonly IUserService userService;
        private readonly ITokenService tokenService;
        private readonly ILogger<AuthenticationConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AuthenticationConsumer"/>.
        /// </summary>
        /// <param name="userService">Сервис для работы с данными пользователей.</param>
        /// <param name="tokenService">Сервис для работы с данными токенов.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public AuthenticationConsumer(IUserService userService, ITokenService tokenService, ILogger<AuthenticationConsumer> logger)
        {
            this.userService = userService;
            this.tokenService = tokenService;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<AuthenticationCommand> context)
        {
            UserDto userFromRequest = context.Message.User;
            this.logger.LogInformation($"Выполняется обработка сообщения аутентификации пользователя с логином '{userFromRequest.Login}'.");

            User existingUser = await this.userService.GetAsync(userFromRequest.Login);

            if (existingUser == null)
            {
                User newUser = new User(userFromRequest.Login, userFromRequest.Password);
                newUser.Id = await this.userService.SaveAsync(newUser);
                string token = this.tokenService.Generate(newUser);

                await context.RespondAsync(new AuthenticationResponse { Token = new TokenDto(token), Result = "success" });
            }
            else
            {
                await context.RespondAsync(new AuthenticationResponse { Result = "conflict" });
            }
        }
    }
}