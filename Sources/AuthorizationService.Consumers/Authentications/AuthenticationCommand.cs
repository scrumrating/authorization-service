﻿using AuthorizationService.Core.Dtos;

namespace AuthorizationService.Consumers.Authentications
{
    /// <summary>
    /// Команда аутентификации пользователя.
    /// </summary>
    public class AuthenticationCommand
    {
        /// <summary>
        /// Получает или задает пользователя.
        /// </summary>
        public UserDto User { get; set; }
    }
}