﻿using AuthorizationService.Core.Dtos;

namespace AuthorizationService.Consumers.Authentications
{
    /// <summary>
    /// Ответ на команду аутентификации пользователя.
    /// </summary>
    public class AuthenticationResponse
    {
        /// <summary>
        /// Получает или задает токен.
        /// </summary>
        public TokenDto Token { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}