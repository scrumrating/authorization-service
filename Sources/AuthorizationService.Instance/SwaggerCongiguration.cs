﻿namespace AuthorizationService.Instance
{
    /// <summary>
    /// Конфигурация swagger.
    /// </summary>
    public class SwaggerCongiguration
    {
        /// <summary>
        /// Получает или задает путь к UI swagger.
        /// </summary>
        public string Uri { get; set; }

        /// <summary>
        /// Получает или задает название.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задает заголовок.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Получает или задает версию.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Получает или задает описание странички.
        /// </summary>
        public string Description { get; set; }
    }
}