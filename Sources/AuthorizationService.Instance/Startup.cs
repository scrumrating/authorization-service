﻿using AuthorizationService.Consumers.Authentications;
using AuthorizationService.Consumers.Authorizations;
using AuthorizationService.Consumers.Healthchecks;
using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Hashers;
using AuthorizationService.Core.Repositories;
using AuthorizationService.Core.Services;
using AuthorizationService.Core.Tokens;
using AuthorizationService.Data.Contexts;
using AuthorizationService.Data.Repositories;
using AuthorizationService.WebApi;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Serilog;
using System;
using System.IO;

namespace AuthorizationService.Instance
{
    /// <summary>
    /// Инициализация приложения.
    /// </summary>
    public class Startup
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Startup"/>.
        /// </summary>
        public Startup(IHostingEnvironment environment)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("appsettings.production.json", true, true);

            this.configuration = builder.Build();
        }

        /// <summary>
        /// Конфигурация приложения.
        /// </summary>
        /// <param name="application">Приложение.</param>
        public void Configure(IApplicationBuilder application)
        {
            this.ConfigureDatabase(application);
            application.UseCors("AllowAll");
            application.UseMvc();
            application.UseSwagger();
            var swaggerCongiguration = this.configuration.GetSection("Swagger").Get<SwaggerCongiguration>();
            application.UseSwaggerUI(c => { c.SwaggerEndpoint(swaggerCongiguration.Uri, swaggerCongiguration.Name); });
        }

        /// <summary>
        /// Конфигурация сервисов.
        /// </summary>
        /// <param name="services">Сервисы.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(this.configuration)
                .CreateLogger();

            Log.Information("Начинается регистрация политик CORS.");

            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            Log.Information("Регистрация политик CORS завершена.");

            Log.Information("Начинается регистрация Swagger генератора.");
            SwaggerCongiguration swaggerCongiguration = this.configuration.GetSection("Swagger").Get<SwaggerCongiguration>();

            var apiInfo = new OpenApiInfo
            {
                Title = swaggerCongiguration.Title,
                Version = swaggerCongiguration.Version,
                Description = swaggerCongiguration.Description,
            };
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(swaggerCongiguration.Version, apiInfo);
                var xmlFile = "AuthorizationService.WebApi.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            Log.Information("Регистрация Swagger генератора завершена.");

            Log.Information("Начинается регистрация сервисов.");
            services.AddDbContext<AuthorizationDbContext>(option => option.UseNpgsql(this.configuration.GetConnectionString("AuthorizationService")), ServiceLifetime.Singleton);

            services.AddSingleton<IEncodingKey, EncodingKey>();
            services.AddSingleton<IHasher, Sha256Hasher>();

            services.AddSingleton<IRepository<User, string>, UsersRepository>();
            services.AddSingleton<IRepository<Token, int>, TokenRepository>();

            services.AddSingleton<IUserService, UserService>();

            int tokenLifetime = this.configuration.GetValue<int>("TokenLifetime");
            services.AddSingleton<ITokenService>(s =>
                new TokenService(
                    s.GetRequiredService<IRepository<Token, int>>(),
                    s.GetRequiredService<IEncodingKey>(),
                    s.GetRequiredService<ILogger<TokenService>>(),
                    tokenLifetime));

            services.AddSingleton<AuthenticationConsumer>();
            services.AddSingleton<AuthorizationConsumer>();
            Log.Information("Регистрация сервисов завершена.");

            Log.Information("Начинается регистрация шины.");

            // Регистрация потребителей сообщений
            services.AddMassTransit(x =>
            {
                x.AddConsumer<HealthcheckConsumer>();
                x.AddRequestClient<AuthenticationCommand>();
                x.AddRequestClient<AuthorizationCommand>();
            });

            // Регистрация шины.
            // Подробнее про регистрацию шины можно почитать здесь: https://masstransit-project.com/usage/
            services.AddSingleton(serviceProvider => Bus.Factory.CreateUsingRabbitMq(configure =>
            {
                BusConfiguration busConfiguration = this.configuration.GetSection("Bus").Get<BusConfiguration>();

                // Конфигурация подключения к шине, включающая в себя указание адреса и учетных данных.
                configure.Host(new Uri(busConfiguration.ConnectionString), host =>
                {
                    host.Username(busConfiguration.Username);
                    host.Password(busConfiguration.Password);

                    // Подтверждение получения гарантирует доставку сообщений за счет ухудшения производительности.
                    host.PublisherConfirmation = busConfiguration.PublisherConfirmation;
                });

                // Добавление Serilog для журналирования внутренностей MassTransit.
                configure.UseSerilog();

                // Регистрация очередей и их связи с потребителями сообщений.
                // В качестве метки сообщения используется полное имя класса сообщения, которое потребляет потребитель.
                configure.ReceiveEndpoint(typeof(HealthcheckCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<HealthcheckConsumer>(serviceProvider);
                    EndpointConvention.Map<HealthcheckCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(AuthenticationCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<AuthenticationConsumer>(serviceProvider);
                    EndpointConvention.Map<AuthenticationCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(AuthorizationCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<AuthorizationConsumer>(serviceProvider);
                    EndpointConvention.Map<AuthorizationCommand>(endpoint.InputAddress);
                });
            }));

            // Регистрация сервисов MassTransit.
            services.AddSingleton<IPublishEndpoint>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<ISendEndpointProvider>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<IBus>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());

            // Регистрация клиентов для запроса данных от потребителей сообщений из api.
            // Каждый клиент зарегистрирован таким образом, что бы в рамках каждого запроса к api существовал свой клиент.
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<HealthcheckCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<AuthenticationCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<AuthorizationCommand>());

            // Регистрация сервиса шины MassTransit.
            services.AddSingleton<IHostedService, BusService>();
            Log.Information("Регистрация шины завершена.");

            Log.Information("Начинается регистрация сервисов MVC.");
            services.AddMvc();
            Log.Information("Регистрация сервисов MVC завершена.");
        }

        /// <summary>
        /// Обновляет или разворачивает базу данных сервиса.
        /// </summary>
        /// <param name="application">Приложение.</param>
        private void ConfigureDatabase(IApplicationBuilder application)
        {
            var context = application.ApplicationServices.GetRequiredService<AuthorizationDbContext>();
            context.Database.Migrate();
        }
    }
}