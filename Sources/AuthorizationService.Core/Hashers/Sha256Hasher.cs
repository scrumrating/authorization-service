﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace AuthorizationService.Core.Hashers
{
    /// <summary>
    /// Хешер.
    /// </summary>
    public class Sha256Hasher : IHasher, IDisposable
    {
        private readonly HashAlgorithm hashAlgorithm;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Sha256Hasher"/>.
        /// </summary>
        public Sha256Hasher()
        {
            this.hashAlgorithm = SHA256.Create();
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.hashAlgorithm.Dispose();
        }

        /// <summary>
        /// Получить хеш.
        /// </summary>
        /// <param name="input">Строка, для которой необходимо посчитать хеш.</param>
        /// <returns>Хеш.</returns>
        public string GetHash(string input)
        {
            var bstringBilder = new StringBuilder();
            byte[] data = this.hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(input));

            for (int i = 0; i < data.Length; i++)
            {
                bstringBilder.Append(data[i].ToString("x2"));
            }

            return bstringBilder.ToString();
        }

        /// <summary>
        /// Проверить хеш.
        /// </summary>
        /// <param name="input">Строка, для которой необходимо посчитать хеш.</param>
        /// <param name="hash">Хеш для сравнения.</param>
        /// <returns>Признак, который показывает соответствует ли хеш строки с переданным хешем.</returns>
        public bool VerifyHash(string input, string hash)
        {
            var hashOfInput = this.GetHash(input);
            return string.Compare(hashOfInput, hash, StringComparison.OrdinalIgnoreCase) == 0;
        }
    }
}