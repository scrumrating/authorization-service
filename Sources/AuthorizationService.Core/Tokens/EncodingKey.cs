﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace AuthorizationService.Core.Tokens
{
    /// <summary>
    /// Ключ для создания цифровой подписи.
    /// </summary>
    public class EncodingKey : IEncodingKey
    {
        private const string AlgorithmValue = SecurityAlgorithms.HmacSha256;
        private const string Key = "someSecretKey123!";

        /// <summary>
        /// Получает алгоритм шифирования.
        /// </summary>
        public string Algorithm => AlgorithmValue;

        /// <summary>
        /// Получает ключ цифровой подписи.
        /// </summary>
        public SymmetricSecurityKey SymmetricKey => new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
    }
}