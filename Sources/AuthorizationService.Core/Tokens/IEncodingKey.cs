﻿using Microsoft.IdentityModel.Tokens;

namespace AuthorizationService.Core.Tokens
{
    /// <summary>
    /// Ключ для создания цифровой подписи.
    /// </summary>
    public interface IEncodingKey
    {
        /// <summary>
        /// Получает алгоритм шифирования.
        /// </summary>
        /// <value>
        /// Алгоритм шифирования.
        /// </value>
        string Algorithm { get; }

        /// <summary>
        /// Получает ключ цифровой подписи.
        /// </summary>
        /// <value>
        /// Ключ цифровой подписи.
        /// </value>
        SymmetricSecurityKey SymmetricKey { get; }
    }
}