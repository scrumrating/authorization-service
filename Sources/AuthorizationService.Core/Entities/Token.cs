﻿using System;

namespace AuthorizationService.Core.Entities
{
    /// <summary>
    /// Токен.
    /// </summary>
    public class Token
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Token"/>.
        /// </summary>
        public Token()
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Token"/>.
        /// </summary>
        /// <param name="value">Значение токена.</param>
        /// <param name="validTo">Дата и время, до которого действует токен.</param>
        /// <param name="userId">Идентификатор пользователя.</param>
        public Token(string value, DateTime validTo, int userId)
        {
            this.Value = value;
            this.ValidTo = validTo;
            this.UserId = userId;
        }

        /// <summary>
        /// Получает или задает идентификатор токена.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Получает или задает значение токена.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Получает или задает дату и время, до которого действует токен.
        /// </summary>
        public DateTime ValidTo { get; set; }

        /// <summary>
        /// Получает или задает идентификатор пользователя.
        /// </summary>
        public int UserId { get; set; }
    }
}