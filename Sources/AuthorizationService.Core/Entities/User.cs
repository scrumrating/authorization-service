﻿using System;
using System.Collections.Generic;

namespace AuthorizationService.Core.Entities
{
    /// <summary>
    /// Пользователь.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="User"/>.
        /// </summary>
        public User()
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="User"/>.
        /// </summary>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        public User(string login, string password)
        {
            this.Login = login;
            this.Password = password;
            this.CreatedOn = DateTime.Now;
        }

        /// <summary>
        /// Получает или задает идентификатор пользователя.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Получает или задает логин пользователя.
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Получает или задает пароль пользователя.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Получает или задает дату и время создания пользователя.
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Получает или задает список токенов пользователя.
        /// </summary>
        public virtual List<Token> Tokens { get; set; }
    }
}