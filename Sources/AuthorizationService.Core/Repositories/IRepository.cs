using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuthorizationService.Core.Repositories
{
    /// <summary>
    /// ����������� ��� �������������� � ������� ��������.
    /// </summary>
    /// <typeparam name="TData">������ ��������.</typeparam>
    /// <typeparam name="TKey">���� ��������, ������� �� ���������� ����������.</typeparam>
    public interface IRepository<TData, TKey>
    {
        /// <summary>
        /// �������� ������ �������� � ���������.
        /// </summary>
        /// <param name="data">�������.</param>
        /// <returns>������������� ��������.</returns>
        int Add(TData data);

        /// <summary>
        /// �������� ������ �������� � ���������.
        /// </summary>
        /// <param name="data">������ ��������.</param>
        /// <returns>����������� ��������.</returns>
        Task<int> AddAsync(TData data);

        /// <summary>
        /// ����� �������� �� � �����.
        /// </summary>
        /// <param name="key">���� ��������, ������� � ���������� ����������.</param>
        /// <returns>�������.</returns>
        TData Find(TKey key);

        /// <summary>
        /// ����� �������� �� � �����.
        /// </summary>
        /// <param name="key">���� ��������, ������� � ���������� ����������.</param>
        /// <returns>�������.</returns>
        Task<TData> FindAsync(TKey key);
    }
}