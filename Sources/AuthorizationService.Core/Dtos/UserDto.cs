﻿using System.ComponentModel.DataAnnotations;

namespace AuthorizationService.Core.Dtos
{
    /// <summary>
    /// Пользователь.
    /// </summary>
    public class UserDto
    {
        /// <summary>
        /// Получает или задает логин.
        /// </summary>
        [Required]
        public string Login { get; set; }

        /// <summary>
        /// Получает или задает пароль.
        /// </summary>
        [Required]
        public string Password { get; set; }
    }
}