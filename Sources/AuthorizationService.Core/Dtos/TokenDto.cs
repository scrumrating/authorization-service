﻿namespace AuthorizationService.Core.Dtos
{
    /// <summary>
    /// Токен.
    /// </summary>
    public class TokenDto
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TokenDto"/>.
        /// </summary>
        /// <param name="token">Токен.</param>
        public TokenDto(string token)
        {
            this.Token = token;
        }

        /// <summary>
        /// Получает или задает токен.
        /// </summary>
        public string Token { get; set; }
    }
}