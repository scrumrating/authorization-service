﻿using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Hashers;
using AuthorizationService.Core.Repositories;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AuthorizationService.Core.Services
{
    /// <summary>
    /// Сервис для работы с данными пользователей.
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IRepository<User, string> repository;
        private readonly IHasher hasher;
        private readonly ILogger<UserService> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="UserService"/>.
        /// </summary>
        /// <param name="repository">Репозиторий.</param>
        /// <param name="hasher">Хешер.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public UserService(IRepository<User, string> repository, IHasher hasher, ILogger<UserService> logger)
        {
            this.repository = repository;
            this.hasher = hasher;
            this.logger = logger;
        }

        /// <summary>
        /// Получить данные пользователя по логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <returns>Пользователь.</returns>
        public async Task<User> GetAsync(string login)
        {
            this.logger.LogInformation($"Выполняется поиск пользователя с логином '{login}'.");
            return await this.repository.FindAsync(login);
        }

        /// <summary>
        /// Получить данные пользователя по логину и паролю.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <param name="password">Пароль пользователя.</param>
        /// <returns>Пользователь.</returns>
        public async Task<User> GetAsync(string login, string password)
        {
            this.logger.LogInformation($"Выполняется поиск пользователя с логином '{login}'.");
            User user = await this.repository.FindAsync(login);

            if (user != null)
            {
                this.logger.LogInformation($"Выполняется хеширование пароля пользователя с логином '{login}'.");
                string passwordHash = this.hasher.GetHash(password);

                user = user.Password.Equals(passwordHash, StringComparison.InvariantCulture) ? user : null;
            }

            return user;
        }

        /// <summary>
        /// Cохранить данные пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task<int> SaveAsync(User user)
        {
            this.logger.LogInformation($"Выполняется хеширование пароля пользователя с логином '{user.Login}'.");
            user.Password = this.hasher.GetHash(user.Password);

            this.logger.LogInformation($"Выполняется сохранение пользователя с логином '{user.Login}'.");
            int userId = await this.repository.AddAsync(user);

            return userId;
        }
    }
}