﻿using AuthorizationService.Core.Entities;
using System.Threading.Tasks;

namespace AuthorizationService.Core.Services
{
    /// <summary>
    /// Сервис для работы с данными токенов.
    /// </summary>
    public interface ITokenService
    {
        /// <summary>
        /// Сгенерировать токен для пользователя.
        /// </summary>
        /// <param name="user">Пользователь, для которого генерируется токен.</param>
        /// <returns>Токен <see cref="Token"/>.</returns>
        string Generate(User user);

        /// <summary>
        /// Получить токен пользователя.
        /// </summary>
        /// <param name="user">Пользователь, для которого необходимо получить токен.</param>
        /// <returns>Токен.</returns>
        Task<string> GetAsync(User user);
    }
}