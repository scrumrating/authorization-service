﻿using AuthorizationService.Core.Entities;
using AuthorizationService.Core.Repositories;
using AuthorizationService.Core.Tokens;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuthorizationService.Core.Services
{
    /// <summary>
    /// Сервис для работы с данными токенов.
    /// </summary>
    public class TokenService : ITokenService
    {
        private readonly IRepository<Token, int> repository;
        private readonly IEncodingKey encodingKey;
        private readonly ILogger<TokenService> logger;
        private readonly int tokenLifetime;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TokenService"/>.
        /// </summary>
        /// <param name="repository">Репозиторий для работы с данными токенов в хранилище.</param>
        /// <param name="encodingKey">Ключ, для создания цифровой подписи.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        /// <param name="tokenLifetime">Время жизни токена в минутах.</param>
        public TokenService(IRepository<Token, int> repository, IEncodingKey encodingKey, ILogger<TokenService> logger, int tokenLifetime)
        {
            this.repository = repository;
            this.encodingKey = encodingKey;
            this.logger = logger;
            this.tokenLifetime = tokenLifetime;
        }

        /// <summary>
        /// Сгенерировать токен.
        /// </summary>
        /// <param name="user">Пользователь, для которого генерируется токен.</param>
        /// <returns>Токен.</returns>
        public string Generate(User user)
        {
            this.logger.LogInformation($"Выполняется генерация токена для пользователя с логином '{user.Login}'");
            var claims = new Claim[] { new Claim(ClaimTypes.Name, user.Login) };

            JwtSecurityToken jwtToken = new JwtSecurityToken(
                issuer: "ScrumRating",
                audience: "ScrumRating",
                claims: claims,
                expires: DateTime.Now.AddMinutes(this.tokenLifetime),
                signingCredentials: new SigningCredentials(this.encodingKey.SymmetricKey, this.encodingKey.Algorithm));

            string tokenValue = new JwtSecurityTokenHandler().WriteToken(jwtToken);
            Token token = new Token(tokenValue, jwtToken.ValidTo, user.Id);

            this.repository.Add(token);

            return tokenValue;
        }

        /// <summary>
        /// Получить токен пользователя.
        /// </summary>
        /// <param name="user">Пользователь, для которого необходимо получить токен.</param>
        /// <returns>Токен.</returns>
        public async Task<string> GetAsync(User user)
        {
            Token token = await this.repository.FindAsync(user.Id);
            return token?.Value;
        }
    }
}