﻿using AuthorizationService.Core.Entities;
using System.Threading.Tasks;

namespace AuthorizationService.Core.Services
{
    /// <summary>
    /// Сервис для работы с данными пользователей.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Получить данные пользователя по логину.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <returns>Пользователь.</returns>
        Task<User> GetAsync(string login);

        /// <summary>
        /// Получить данные пользователя по логину и паролю.
        /// </summary>
        /// <param name="login">Логин пользователя.</param>
        /// <param name="password">Пароль пользователя.</param>
        /// <returns>Пользователь.</returns>
        Task<User> GetAsync(string login, string password);

        /// <summary>
        /// Cохранить данные пользователя.
        /// </summary>
        /// <param name="user">Пользователь.</param>
        /// <returns>Асинхронная операция.</returns>
        Task<int> SaveAsync(User user);
    }
}